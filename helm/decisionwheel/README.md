# shared decision wheel

## install minimal

`helm install wheel .`

## ingress/certmanager support

`kubectl create ns wheel`

`helm upgrade --set ingress.hosts[0]=wheel.esidata.de -n wheel wheel .`