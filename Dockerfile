FROM node:11-alpine as prod
EXPOSE 80
WORKDIR /app
COPY src /app
RUN npm install && \
    npm run build
    
ENTRYPOINT node server.js