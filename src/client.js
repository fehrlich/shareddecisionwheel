//var DiffSyncClient = require('diffsync').Client
//
//// socket.io standalone or browserify / webpack
//var socket = window.io || require('socket.io-client')
//
//// pass the connection and the id of the data you want to synchronize
//var client = new DiffSyncClient(socket(), 'test');
//
//var data;
//
//client.on('connected', function () {
//    // the initial data has been loaded,
//    // you can initialize your application
//    model = client.getData();
//    if (typeof App != 'undefined') {
//        App.getObj().getControler().updateModel(model);
//    }
//});
//
//function submitObject(model){
//    data = model;
//    client.storeData('model', model);
//    client.sync();
//};
//
//client.on('synced', function () {
//    console.log('wait what');
//});
//
//client.initialize();
//
///* --- somewhere in your code --- */
//
////data.randomChange = Math.random();
//// schedule a sync cycle - this will sync your changes to the server
////client.sync();
//
//global.currentClient = client;


var sharedb = require('sharedb/lib/client');
var doc;

function initModel(id){
    // Open WebSocket connection to ShareDB server
    var socket = new WebSocket('ws://' + window.location.host + '/'+id, id);
    var connection = new sharedb.Connection(socket);

    // Create local Doc instance mapped to 'examples' collection document with id 'counter'
    doc = connection.get('wheel', id);

    // Get initial value of document and subscribe to changes
    doc.subscribe(updateModel);
    // When document changes (by this client or any other, or the server),
    // update the number on the page
    doc.on('op', updateModel);
}

function updateModel() {
    let model = doc.data.model;
    if(typeof App != 'undefined'){
        App.getObj().getControler().updateModel(model);        
    }
};

// When clicking on the '+1' button, change the number in the local
// document and sync the change to the server and other connected
// clients
function submitOperation(otOperation) {
  // Increment `doc.data.numClicks`. See
  // https://github.com/ottypes/json0 for list of valid operations.
  doc.submitOp(otOperation);
}

// Expose to index.html
global.submitOperation = submitOperation;
global.updateModel = updateModel;
global.initModel = initModel;
