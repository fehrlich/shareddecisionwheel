var http = require('http');
var express = require('express');
var ShareDB = require('sharedb');
var WebSocket = require('ws');
var WebSocketJSONStream = require('websocket-json-stream');

var backend = new ShareDB();
//createDoc(startServer);

function srandomColor() {
    var hexLetters = '23456789ABCDEF';
    var color = '';

    for (var i = 0; i < 6; i++) {
        color += hexLetters[Math.floor(Math.random() * 14)];
    }

    return color;
}

// Create initial document then fire callback
function createDoc(callback) {
    var connection = backend.connect();
    var doc = connection.get('wheel', 'test');
    doc.fetch(function (err) {
        if (err)
            throw err;
        if (doc.type === null) {
            doc.create({
                'model': {
                    'decision': null,
                    'turns': [],
                    'options': [
                        {
                            'title': 'CS',
                            'color': srandomColor()
                        },
                        {
                            'title': 'RL',
                            'color': srandomColor()
                        },
                        {
                            'title': 'Aram',
                            'color': srandomColor()
                        },
                        {
                            'title': 'BP',
                            'color': srandomColor()
                        },
                        {
                            'title': 'Argo',
                            'color': srandomColor()
                        },
                        {
                            'title': 'DST',
                            'color': srandomColor()
                        },
                        {
                            'title': 'Minion Master',
                            'color': srandomColor()
                        },
                        {
                            'title': 'Ghost Recon',
                            'color': srandomColor()
                        },
                        {
                            'title': 'Team Fortress',
                            'color': srandomColor()
                        },
                        {
                            'title': 'Trackmania',
                            'color': srandomColor()
                        }
                    ]
                }
            }, callback);
            return;
        }
        callback();
    });
}

function startServer() {
    // Create a web server to serve files and listen to WebSocket connections
    var app = express();
    app.use(express.static('static'));
    var server = http.createServer(app);

    // Connect any incoming WebSocket connection to ShareDB
    var wss = new WebSocket.Server({
        server: server,
    });
    wss.on('connection', function (ws, req) {
        console.log(ws);
        var stream = new WebSocketJSONStream(ws);
        backend.listen(stream);
    });
    var port = 80;
    server.listen(port);
    console.log('Listening on http://localhost:'+port);
}


var connection = backend.connect();

function initBackend(callback){
    var doc = connection.get('wheel', 'test');
    doc.fetch(function (err) {
        if (err)
            throw err;
        callback();
    });
}

function getWheel(id){
    
    var doc = connection.get('wheel', id);
    if (doc.type === null) {
        doc.create({
            'model': {
                'decision': null,
                'turns': [],
                'options': [
                    {
                        'title': 'CS',
                        'color': srandomColor()
                    },
                    {
                        'title': 'RL',
                        'color': srandomColor()
                    },
                    {
                        'title': 'Aram',
                        'color': srandomColor()
                    },
                    {
                        'title': 'BP',
                        'color': srandomColor()
                    },
                    {
                        'title': 'DST',
                        'color': srandomColor()
                    },
                    {
                        'title': 'LOL',
                        'color': srandomColor()
                    },
                    {
                        'title': 'TFT',
                        'color': srandomColor()
                    }
                ]
            }
        });
        return;
    }
}

function start(){
    
    // Create a web server to serve files and listen to WebSocket connections
    var app = express();
    app.use(express.static('static'));
    var server = http.createServer(app);
    
    
    app.get('/wheel_*', function (req, res) {
        var code = req.params[0];
        console.log("Request", code);
//        res.send('nah');
        res.sendFile('./index.html', { root: __dirname });
    });

    // Connect any incoming WebSocket connection to ShareDB
    var wss = new WebSocket.Server({server: server});
    wss.on('connection', function (ws, req) {
        console.log(ws.protocol);
        var wheelId = ws.protocol ? ws.protocol : 'test';
        getWheel(wheelId);
        var stream = new WebSocketJSONStream(ws);
//        console.log('stream', ws);
        console.log('req', wheelId);
        backend.listen(stream);
    });

    var port = 80;
    server.listen(port);
    console.log('Listening on http://localhost:'+port);
}

initBackend(start);
//start();