define(function(){
    function App(){
        this.controler = null;        
    };
    
    App.prototype = {
        'constructor': App,
        
        'setControler': function(controler){
            this.controler = controler;
        },
        
        'getControler': function(){
            return this.controler;
        }
    };
    
    App.handle = new App();

    App.getObj = function(cb){
        return App.handle;
    };
    
    window.App = App;
    
    return App;
});