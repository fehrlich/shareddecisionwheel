define([
    'colorScheme'
], function () {
    //<editor-fold defaultstate="collapsed" desc="colors">
//    TODO: outsource
    var ColorScheme  = require('colorScheme');
    var scheme = new ColorScheme;
          scheme.from_hue(256)         // Start the scheme 
          .scheme('tetrade')     // Use the 'triade' scheme, that is, colors
                                // selected from 3 points equidistant around
                                // the color wheel.
          .variation('pastel');   // Use the 'soft' color variation

    var colors = scheme.colors();
    
    function randomColor(){
        var hexLetters = '23456789ABCDEF';
        var color = '';
        
        for (var i = 0; i < 6; i++) {
          color += hexLetters[Math.floor(Math.random() * 14)];
        }
        
        return color;
    }
    
    function getColor(index){
        if(colors[index] && false){
            return ''+colors[index];
        }else{
            return ''+randomColor();
        }
    }
    //</editor-fold>

    function Option(title, index, color, disabled) {
        index = index || 0;
        this.title = title;
        this.color = color ? color : getColor(index);
        this.index = index;
        this.disabled = !!disabled;
        
        this.otPath = ['model', 'options', index];
    }

    Option.prototype = {
        constructor: Option,
        
        'setTitle': function (title) {
            Collab.replace(this, 'title', title);   
        },
        
        'getTitle': function () {
            return this.title;
        },
        
        'getColor': function () {
            return this.color;
        },
        'setColor': function (color) {
            Collab.replace(this, 'color', color);   
        },
        'getIndex': function () {
            return this.index;
        },
        'toggleDisabled': function (){
            Collab.replace(this, 'disabled', !this.disabled);   
        },
        
        'toObj': function () {
            return{
                title: this.title,
                color: this.color
            }
        },
    };

    return Option;
});