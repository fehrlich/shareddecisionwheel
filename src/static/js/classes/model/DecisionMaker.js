define([
    'model/Option'
], function (Option) {
    
    function DescisionMaker(){
        this.decision = null;
        this.turn = [];
        this.options = [];
        this.onChangeCb = null;
        
        this.otPath = ['model'];
    };
        
    DescisionMaker.createFromArray = function(array){
        var dm = new DescisionMaker();
        dm.updateOptionsByArray(array);
        return dm;
    };
    
    DescisionMaker.prototype = {
        constructor: DescisionMaker,
        
        'getOptions': function(filterDisabled) {
            filterDisabled = typeof filterDisabled == "undefined" ? true : false;
            if(filterDisabled){
                return this.options.filter(function(el){
                    return !el.disabled;
                });
            }
            return this.options;
        },
        
        'getDecision': function() {
            return this.decision;
        },
        
        'setDecision': function(decision){
            Collab.replace(this, 'decision', decision);   
            this.submitTurn(decision);         
        },
        
        'getTurns': function(){
            return this.turns;
        },
        
        
        'makeDecision': function () {
            var possOptions = this.getOptions();
            var length = possOptions.length-1;
            var optionIndex = Math.round(Math.random() * length);
            var selectedOption = possOptions[optionIndex];
            
            this.setDecision(selectedOption.index);
        },
        
        

        'updateOptionsByArray': function (array) {
            this.options = [];
            for(var index in array){
                this.addOptionByTitle(array[index]);
            }
        },
        
        'updateOptionsByObject': function (objects) {
            this.options = [];
            for(var index in objects){
                var obj = objects[index];
                var col = obj.color ? obj.color : null;
                this.addOptionByTitle(obj.title, col, obj.disabled);
            }
        },
        
        'submitTurn': function(turn){
            Collab.pushToArray(this, 'turns', turn);
        },
        
        'submitOption': function(option){
            Collab.pushToArray(this, 'options', option);
            
//            this.triggerChange();
        },
        'submitOptionByTitle': function(title, color){
            this.submitOption(new Option(title, this.options.length, color));
        },
        'removeOption': function(index){
            Collab.removeFromArray(this, 'options', index);
        },
        
        'addOptionByTitle': function(title, color, disabled){
            this.options.push(new Option(title, this.options.length, color, disabled));
        },
        
        'publishWheel': function(){
            
        },
        
        triggerChange: function(){
            if(this.onChangeCb){
                this.onChangeCb();
            }
        },
        onChange: function(onChangeCb){
            this.onChangeCb = onChangeCb;
        },
        'toObj': function(){
            var options = [];
            
            for(var option of this.options){
                options.push(option.getTitle());
            }
            
            return {
                decision: this.decision,
                options: options,
            };
        },
        'updateFromObj': function(modelObj){
            this.decision =  modelObj && modelObj.decision ? modelObj.decision : null;
            this.turns =  modelObj && modelObj.turns ? modelObj.turns : null;
            if(modelObj && modelObj.options){
                this.updateOptionsByObject(modelObj.options);
            }
            this.triggerChange();
        }
    };
    
    return DescisionMaker;
});