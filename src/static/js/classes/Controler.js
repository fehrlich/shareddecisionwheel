define(function(){
    function Controler(model, views){
        model = model || Controler.initModel;
        this.model = model;
        this.views = views;
               
        
        for(let view of views){
            view.setModel(model);
        }
        
        this.model.onChange(function(){
            requestAnimationFrame(function(){
                for(let view of views){
                    view.draw();
                }
            });
        });
        
        $(document).on('click', '.makeDecision', function(){
            model.makeDecision();
        });
        
    };
        
    Controler.initModel = null;
    
    Controler.prototype = {
        constructor: Controler,
        
        'updateModel': function(modelObj){
            this.model.updateFromObj(modelObj);
        }
    };
    
    return Controler;
});