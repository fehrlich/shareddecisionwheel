define(
        ['functions/text']
        , function () {

            function DecisionList(listId) {
                this.model = null;
                this.listId = listId;
            };

            DecisionList.prototype = {
                'draw': function(){
                    let id = this.listId;
                    let list = $('#'+id);
                    let model = this.model;
                                        
                    var newList = $('<ul>')
                        .attr('id', id);
                
                
                    var lastElement = false;
                    for(var turnIndex in model.turns){
                        var decisionIndex = model.turns[turnIndex];
                        var el = $('<li>');
                        var option = this.model.getOptions(false)[decisionIndex];
                        if(option){
                            el.text(option.getTitle());
                        }else{
                            el.text('[gelöscht]');                     
                        }
                        
                        if(turnIndex == model.turns.length-1){
                            lastElement = el;
                        }else{
                            newList.prepend(el);                            
                        }
                    }
                    
                    setTimeout(function(){
                        newList.prepend(lastElement);                          
                    },3250);
                    
                    list.replaceWith(newList);
                },
                'setModel': function (model) {
                    this.model = model;
                },
            };


            return DecisionList;
        });