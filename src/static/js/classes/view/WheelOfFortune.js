define(
        ['functions/text']
        , function () {

            function WheelOfFortune(canvas, options) {
                options = options || {};
                canvas.prop('class', 'makeDecision');
                let width = options.width || canvas.parent().width();
                let height = options.height || canvas.parent().height();
                let radius = options.radius || Math.min(width, height) / 2;

                this.model = null;
                
                /**
                 * is used to add an initial startvalue before the rotation, so 
                 * the visual result matches the result given by the server
                 */
                this.rotationAddStartValue = 0;
                /**
                 * how many rotation steps should be made
                 */
//                this.rotationMaxSteps = 2;
                this.rotationMaxSteps = 200;
                /**
                 * current rotation step, if > 0 the wheel is turning
                 */
                this.rotationSteps = 0;
                /**
                 * smoothing factor for the rotation
                 */
//                this.rotationSmoothness = 256;
                this.rotationSmoothness = 10000000;
                /**
                 * start rotation speed of the wheel (in rad per frame)
                 */
                this.rotationStartSpeed = Math.PI;
                /**
                 * the curent rotation state in rad
                 */
                this.rotationState = 0;
                
                /**
                 * the current turn count, measure to stop turning
                 */
                this.turnCount = 0;
                                
                this.circle = {
                    x: 0,
                    y: 0,
                    radius: radius
                };

                this.canvas = canvas;
                this.context = canvas[0].getContext('2d');

                this.setWidth(width);
                this.setHeight(height);
            };

            WheelOfFortune.prototype = {

                'init': function (canvas, options) {
                    this.draw();
                },

                'setModel': function (model) {
                    this.model = model;
                },

                'calcCenter': function () {
                    let x = this.width / 2;
                    let y = this.height / 2;
                    this.circle.x = x;
                    this.circle.y = y;
                },

                'setWidth': function (width) {
                    this.width = width;
                    this.canvas.width(width);
                    this.canvas.prop('width', width);
                    this.calcCenter();
                },

                'setHeight': function (height) {
                    this.height = height;
                    this.canvas.height(height);
                    this.canvas.prop('height', height);
                    this.calcCenter();
                },
                
                'drawArrow': function(){
                    let 
                        rotationSpan = Math.PI/90,
                        width = 20,
                        ctx = this.context,
                        circle = this.circle
                    ;
                    
//                    ctx.save();
                    ctx.fillStyle = "black";

                    ctx.beginPath();
                    ctx.moveTo(circle.x + (circle.radius-width)*Math.cos(this.rotationState), circle.y+(circle.radius-width)*Math.sin(this.rotationState));
                    ctx.lineTo(circle.x+circle.radius*Math.cos(this.rotationState+rotationSpan), circle.y+circle.radius*Math.sin(this.rotationState+rotationSpan));
                    ctx.lineTo(circle.x+circle.radius*Math.cos(this.rotationState-rotationSpan), circle.y+circle.radius*Math.sin(this.rotationState-rotationSpan));
                    
                    ctx.closePath();
                    ctx.fillStyle = "black";
                    ctx.fill();
                    
                    ctx.beginPath();
                    ctx.moveTo(circle.x, circle.y);
                    ctx.lineTo(circle.x+circle.radius*Math.cos(this.rotationState), circle.y+circle.radius*Math.sin(this.rotationState));
                    ctx.stroke();
                },
                
                'rotateCanvas': function(){
                    let ctx = this.context;
                    let circle = this.circle;
                    
                    if(this.rotationSteps > 0){
                        ctx.translate(circle.x, circle.y);
                        
                        /*
                         * (1/b)^(x+a) + c
                         * b ... smoothness
                         * a ... depends on startvalue
                         * c ... depends on steps
                         */
                        let maxSpeed = this.rotationStartSpeed;
                        let maxSteps = this.rotationMaxSteps;
                        let smoothness = this.rotationSmoothness;
                        
                        let x = maxSteps-this.rotationSteps;
                        let a = maxSpeed;
                        let c = -Math.pow(smoothness, 1/(a+maxSteps));
                        
                        let speed = Math.pow(smoothness, 1/(x+a)) + c;
                        
                        if(this.rotationAddStartValue > 0){
                            speed -= this.rotationAddStartValue;
                        }
                        
                        this.rotationState -= speed;
                        this.rotationState = (this.rotationState % (2*Math.PI));
                        this.rotationAddStartValue = 0;
                                                
                        ctx.rotate(speed);
                        ctx.translate(-circle.x, -circle.y);
                    }
                },
                                
                'drawCircle': function(){
                    
                    let ctx = this.context;
                    let circle = this.circle;
                    let previousRadian = -Math.PI * 0.5;
                    let model = this.model;
                    let options = model.getOptions();
                    let total = options.length;
                    let part = 1;
                    var radian = (Math.PI * 2) * (part / total);
                    
                    ctx.save();
                    
                    for (let index in options) {
                        let option = options[index];
                        let color = '#'+option.getColor();
                        
                        let labelText = option.getTitle()+" ";
                        let fontSize = circle.radius / (10*(Math.log(labelText.length / 2)));
                        let textMeasure = measureHeight(fontSize + "px Arial", fontSize, labelText);

                        //Pie-Part
                        ctx.beginPath();
                        ctx.fillStyle = color;
                        ctx.moveTo(circle.x, circle.y);
                        ctx.arc(circle.x, circle.y, circle.radius - 2, previousRadian, previousRadian + radian, false);
                        ctx.closePath();
                        ctx.fill();
                        ctx.save();

                        //Label
                        ctx.fillStyle = "black";
                        ctx.font = fontSize + "px Arial";
                        ctx.translate(circle.x, circle.y);
                        ctx.rotate(previousRadian + 0.5 * radian);
                        ctx.fillText(labelText, circle.radius - ctx.measureText(labelText).width - 20, textMeasure.height / 2);
                        ctx.restore();

                        previousRadian += radian;
                    }
                    ctx.restore();
                },
                
                'rotateReset' : function(rad){
                    let ctx = this.context;
                    let circle = this.circle;
                    
                    ctx.translate(circle.x, circle.y);

                    ctx.rotate(this.rotationState);
                    this.rotationState = 0;
                    this.rotationState = (this.rotationState % (2*Math.PI));
                    ctx.translate(-circle.x, -circle.y);
                },
                
                'rotateToRad' : function(rad){
                    let ctx = this.context;
                    let circle = this.circle;
                    
                    ctx.translate(circle.x, circle.y);

                    ctx.rotate(rad - this.rotationState);
                    this.rotationState = -rad;
                    this.rotationState = (this.rotationState % (2*Math.PI));
//                    ctx.rotate(rad);
//                    this.rotationState -= (rad);
//                    this.rotationState = (this.rotationState % (2*Math.PI));
                    ctx.translate(-circle.x, -circle.y);
                },

                'draw': function () {
                    let dpi = window.devicePixelRatio;
                    let ctx = this.context;
                    let self = this;
                    
                    ctx.clearRect(0, 0, this.width, this.height);
                    
                    this.rotateCanvas();
                    
//                    circle = this.circle;
//                    ctx.translate(circle.x, circle.y);
//                    ctx.rotate(Math.PI);
//                    ctx.translate(-circle.x, -circle.y);
                    
                    this.drawCircle();
//                    
                    this.drawArrow();

//                    ctx.scale(dpi, dpi);
                    
                    if(this.rotationSteps > 0){
                        this.rotationSteps--;
                        if(this.rotationSteps == 1){
                            this.showCurrentItem();
                        }
                        requestAnimationFrame(function(){
                            self.draw();
                        });
                    }
                    
                    let modelDecision = this.model.getDecision();
                    
                    if(this.turnCount !== this.model.getTurns().length){
                        this.turnCount = this.model.getTurns().length;
                        this.rotate(modelDecision);
                    }
                    
//                    ctx.restore();
                },
                
                'getButton': function(){
                    return this.canvas;
                },
                
                'showCurrentItem': function(){
                    let shownIndex = this.getCurrentItem();
                    let index = this.model.decision;
                    let item = this.model.options[index];
                    
                    if(shownIndex != index){
                        console.log(index, shownIndex);
                        console.error('Shown Item does not represent the actual value!');
                    }
                },
                'getCurrentItem': function(){
                    let options = this.model.options;
                    
                    let maxLength = options.length-1;
                    let factor = ((2.5*Math.PI + this.rotationState))/(2*Math.PI);
                    
                    if(factor > 1){
                        factor--;
                    }
                    
                    let itemIndex = Math.round(maxLength*factor);
                                        
                    return itemIndex;
                },
                
                'getRotationDiff': function(){
                    
                    let maxSteps = this.rotationMaxSteps;
                    let speed = 0;
                    for(var x = 0; x <= maxSteps ; x++){
                        let maxSpeed = this.rotationStartSpeed;
                        let smoothness = this.rotationSmoothness;
                        
                        let a = maxSpeed;
                        let c = -Math.pow(smoothness, 1/(a+maxSteps));
                        
                        let currentSpeed = Math.pow(smoothness, 1/(x+a)) + c;
                        speed += currentSpeed;
                    }
                    return speed % (Math.PI*2);
                },
                
                'getRotatinIndexCount': function(){
                    return 19;
                },
                
                'rotate': function(endIndex){
                    let options = this.model.options;
                    let itemCount = options.length;
                    let itemRad = (2*Math.PI)/itemCount;
//                    this.rotateToRad(0.5*Math.PI - itemRad/2);
                    let currentItemIndex = this.getCurrentItem();
                    let indexDiff = endIndex;
//                    let indexDiff = endIndex - currentItemIndex;
                    indexDiff = indexDiff < 0 ? indexDiff + itemCount : indexDiff;
                    let indexDiffRad = indexDiff*itemRad;
                    
                    let rand = Math.random()-0.5;
                    let randomVariation = itemRad*rand;
                    this.rotationState = (this.rotationState % (2*Math.PI));
                                        
                    this.rotationAddStartValue = 
                            -this.rotationState  //reset current rotation
                            -(0.5*Math.PI - itemRad/2)  //rotate for a quarter + half an item, because first item is on the top going left
                            + indexDiffRad  //the rotation we need to go
                            + this.getRotationDiff() // fixed rotation index
                            + randomVariation //add a random variance between [-0.5, 0.5] of itemwidth
                    ;
                    
                    this.rotationAddStartValue = (this.rotationAddStartValue % (2*Math.PI));;
                    
                    this.rotationSteps = this.rotationMaxSteps;
                    
                    this.draw();                    
                }
            };

            return WheelOfFortune;
        });