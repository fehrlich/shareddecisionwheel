define(
        ['functions/text']
        , function () {

            function DecisionList(listId) {
                this.model = null;
                this.listId = listId;
            };

            DecisionList.prototype = {
                'getInputField': function(option){
                    var self = this;
                    var wrap = $('<li>');
                    
                    var titleInp = $('<input>')
                        .prop('type', 'text')
                        .css('text-decoration', option.disabled ? 'line-through' : '')
                        .prop('value', option.getTitle())
                        .change(function(){
                            option.setTitle($(this).val())
                        });
                    
                    var deleteInp = $('<button>')
                    .html('delete')
                    .click(function(){
                        self.model.removeOption(option.getIndex());
                    });
                    
                    var disableInp = $('<button>')
                        .html('disable ')
                        .click(function(){
                            option.toggleDisabled();
                        });
                    
                    wrap.append(titleInp);
                    wrap.append(disableInp);
                    wrap.append(deleteInp);
                    
                    return wrap;
                },
                'getAddField': function(){
                    var self = this;
                    var wrap = $('<li>');
                    
                    var addInp = $('<button>')
                        .html('add')
                        .click(function(){
                            self.model.submitOptionByTitle("");
                        })
                    ;
                    
                    wrap.append(addInp);
                    return wrap;
                },
                
                'getPublish': function(){
                    var self = this;
                    var wrap = $('<li>');
                    
                    var addInp = $('<button>')
                        .html('publish')
                        .click(function(){
                            self.model.submitOptionByTitle("");
                        })
                    ;
                    
                    wrap.append(addInp);
                    
                    return wrap;
                },
                
                'draw': function(){
                    let id = this.listId;
                    let list = $('#'+id);
                    let model = this.model;
                                        
                    var newList = $('<ul>')
                        .attr('id', id);
                
                    for(var option of model.options){
                        var el = this.getInputField(option);
                        newList.append(el);
                    }
                    
                    newList.append(this.getAddField());
                    list.replaceWith(newList);
                },
                
                'setModel': function (model) {
                    this.model = model;
                },
            };


            return DecisionList;
        });