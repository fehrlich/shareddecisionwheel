define(function () {
    function Collab(model, view) {

    };

    Collab.replace = function (object, field, value) {
        var path = object.otPath.slice();
        path.push(field);

        var otOperation = {
            p: path,
            od: object[field]
        };

        object[field] = value;


        otOperation.oi = value;
        submitOperation(otOperation);
    };
    
    Collab.pushToArray = function (object, field, value) {
        var path = object.otPath.slice();
        path.push(field);
        path.push(object[field].length);

        if(value.toObj){
            value = value.toObj();
        }
        
        var otOperation = {
            p: path,
            li: value
        };
                        
        submitOperation(otOperation);
    };
    
    Collab.removeFromArray = function (object, field, index) {
        var path = object.otPath.slice();
        path.push(field);
        path.push(index);
        
        var val = object[field][index];
        if(val.toObj){
            val = val.toObj();
        }

        var otOperation = {
            p: path,
            ld: object[field][index]
        };
        
        submitOperation(otOperation);
    };

    window.Collab = Collab;
    return Collab;
});