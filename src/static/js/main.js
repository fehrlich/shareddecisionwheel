requirejs.config({
    baseUrl: 'js/classes',
    paths: {
        // the left side is the module ID,
        // the right side is the path to
        // the jQuery file, relative to baseUrl.
        // Also, the path should NOT include
        // the '.js' file extension. This example
        // is using jQuery 1.9.0 located at
        // js/lib/jquery-1.9.0.js, relative to
        // the HTML page.
        'jquery': '../vendor/jquery-1.11.2',
        'colorScheme': '../vendor/color-scheme.min',
    }
});

var qs = (function(a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
        var p=a[i].split('=', 2);
        if (p.length == 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4()+s4();
}

function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var wheelView = 0;

require([
    'model/DecisionMaker',
    'view/WheelOfFortune',
    'view/DecisionList',
    'view/OptionList',
    'Controler',
    'App',
    'Collab',
], function(DescisionMaker, WheelOfFortune, DecisionList, OptionList, Controler,App){
    var wheel = qs.wheel ? qs.wheel : 'new';
    var savedWheels = readCookie('wheels') ? JSON.parse(readCookie('wheels')) : {};
    for(var key in savedWheels){
        var delWheel = $(
            '<span class="btn btn-danger" data-key="'+key+'">X</span> '
        );
        var btnGroup = $('<div class="btn-group">'+
            '<a href="?wheel='+key+'" class="btn btn-default">'+savedWheels[key]+'</a>'+
            '</div>');
        delWheel.click(function(){
            var key = $(this).data('key');
            delete savedWheels[key];
            createCookie('wheels', JSON.stringify(savedWheels));
            $(this).parent().remove();
        });
        btnGroup.append(delWheel);
        $('#savedWheels').append(btnGroup);
    }
    var loc = document.location;
    var wheellink = loc.protocol + '//' + loc.host + '?wheel=' + wheel;
    var newWheel = loc.protocol + '//' + loc.host + '?wheel=' + guid();
    $('#wheelUrl').val(wheellink);
    $('#wheelLink').prop('href', wheellink);
    $('#wheelNewLink').prop('href', newWheel);
    $('#wheelSave').click(function(){
        var name = prompt('wheel name');
        savedWheels[wheel] = name;
        createCookie('wheels', JSON.stringify(savedWheels));
    });
    
    var model = DescisionMaker.createFromArray();
    var view1 = new WheelOfFortune($('#wheelOfFortune'));
    var view2 = new DecisionList('decisionList');
    var view3 = new OptionList('optionList');
    var controler = new Controler(model, [view1, view2, view3]);
    App.getObj().setControler(controler);
    
    initModel(wheel);
//    updateModel();
    wheelView = view1;
});