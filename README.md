# shared decision wheel

## install

### helm

## helm with certmanager and namespace

`helm repo add wheel https://fehrlich.gitlab.io/shareddecisionwheel/helm_repo`
`helm repo update`
`kubectl create ns wheel`
`helm install --set ingress.hosts[0]=wheel.mydomain.de -n wheel --version 0.1.1 wheel/decisionwheel`

if certmanager is installed, it will auto generate a cert for the giving host